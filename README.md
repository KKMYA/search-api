Introduction
============

This API is part of toobib.org projet. 
It will be served on server provided by [interhop.org](interorg.org)

## Data sources

- [annuaire.sante.fr](https://annuaire.sante.fr/web/site-pro)
> Près de 200.000 professionnels ADELI, incluant des acteurs clés du médico-social, ont récemment été intégrés dans l’Annuaire Santé, et donc intégrés dans les extractions : cela concerne les psychologues, assistants de service social, ostéopathes, psychothérapeutes, chiropracteurs, assistants dentaires, et les physiciens médicaux.
There is RPPS number (unique professional number). This dataset is our reference.
- [Annuaire santé de la Cnam](https://www.data.gouv.fr/fr/datasets/annuaire-sante-de-la-cnam/#_)
> Comme expliqué, les données sont issues de traitements statistiques sur l’activité des professionnels. Avec des règles de seuils et de filtres. Les actes non pratiqués ou en deçà du seuil de significativité sur la période considérée ne produisent donc aucune activité pour le professionnel en question sur cette période.
Look to dirty to use the source
Other links [Annuaire Santé : Liste, localisation et tarifs des professionnels de santé ](https://public.opendatasoft.com/explore/dataset/medecins/information/?dataChart=eyJxdWVyaWVzIjpbeyJjb25maWciOnsiZGF0YXNldCI6Im1lZGVjaW5zIiwib3B0aW9ucyI6eyJxIjoiVEFIQVIgYWxiZXJ0In19LCJjaGFydHMiOlt7ImFsaWduTW9udGgiOnRydWUsInR5cGUiOiJjb2x1bW4iLCJmdW5jIjoiQVZHIiwieUF4aXMiOiJjb2x1bW5fMTEiLCJzY2llbnRpZmljRGlzcGxheSI6dHJ1ZSwiY29sb3IiOiIjRkY1MTVBIn1dLCJ4QXhpcyI6ImNpdmlsaXRlIiwibWF4cG9pbnRzIjo1MCwic29ydCI6IiJ9XSwidGltZXNjYWxlIjoiIiwiZGlzcGxheUxlZ2VuZCI6dHJ1ZSwiYWxpZ25Nb250aCI6dHJ1ZX0%3D&location=2,30.51163,0&basemap=jawg.streets)
- [Base officielle des codes postaux ](https://datanova.laposte.fr/explore/dataset/laposte_hexasmal/information/?disjunctive.code_commune_insee&disjunctive.nom_de_la_commune&disjunctive.code_postal&disjunctive.ligne_5)
From "La Poste"
Useful to get latitude and longitude
- [FINESS Extraction du Fichier des établissements](https://www.data.gouv.fr/fr/datasets/finess-extraction-du-fichier-des-etablissements/#_)
List of establishments in the health and social field.


Usage
============


The production API is currently available here: {{url}} = xxx

The development API is currently available here: {{url}} = xxx

## url for the development API:

- endpoints:
  - Hello_world : Full text search on last name, first name, city, Profession. You can also search for specific last name, first name, city, Position, National identification number (inpp).
    - GET method : {{url}}/providers?search=value?key1=value1?key2=value2
    - Examples:
      - {{url}}/providers?search=adrien parrot
      - {{url}}/providers?search=Martin&nom=Dupont&prenom=Louis&commune=Paris&profession=Infirmier
      - {{url}}/providers?inpp=810100965150
<!--    - {{url}}/providers?inpp=[810100965150, 833333232]
        - Search a provider by toobib id : {{url}}/providers/id=[4086690, 4333333]

  - GET Choose a specific model
   - {{url}}/providers?search=Martin&nom=Dupont&prenom=Louis&commune=Paris&profession=Infirmier&model=[EXTERNAL, INTERNAL]
    - {{url}}/providers?inpp=[810100965150, 833333232]&model=[EXTERNAL]
-->

  - Provider : Get one practioner based on its identifier number
    - GET method: {{url}}/provider?inpp=number
    - Example: 
      - {{url}}/provider?inpp=810100965150

Installation
===============

#### Database
- Installation postgreSQL

- connect to postgres
```bash
sudo -u postgres psql
```

- Create user and database
```psql
CREATE USER "USER" WITH PASSWORD "PWD" CREATEDB;
CREATE DATABASE "DB" OWNER "USER";
```

- Install the extensions pg_trgm and unaccent:
```psql
\c "DB"
CREATE EXTENSION unaccent;
CREATE EXTENSION pg_trgm;
ou CREATE EXTENSION pg_trgm with schema public;
```

- Exit psql and edit macros SCHEMA, USER, DB in Makefile

- create a [Config](https://wiki.postgresql.org/wiki/Pgpass) .pgpass file in your home. In it write the following

```bash
"IP":"PORT":"DB":"USER":"PWD"
```

example: 
```bash
localhost:5432:toobib:toobib:password
```
- apply the correct right to .pgpass file
```bash
sudo chmod 600 ~/.pgpass
```

- Run Makefile
```bash
make all
```

if your db is empty after the make, you might need to replace, in the file ./db-builder/annuaire_sante_load_api.sql, 'public.unaccent' by 'unaccent'.

#### Run Api Server

- launch python interpretator:

``` bash
python
```
- generate secret key

``` python
# To generate a new secret key:
# >>> import random, string
# >>> secret_key = "".join([random.choice(string.printable) for _ in range(24)])
# >>> jwt_secret_key = "".join([random.choice(string.printable) for _ in range(24)])
# >>> secret_key + '  ' + jwt_secret_key

```

copy the value of secret_key et jwt_secret_key to paste it in a config.py file, that we create in the next step.

- create config.py file for flask server in the app_flask folder
```bash
vim app_flask/config.py
```

- paste this code in the file config.py file
``` python
class Config(object):
    SECRET_KEY = secret_key # paste here the value of secret_key

    SQLALCHEMY_TRACK_MODIFICATIONS = False
    PROPAGATE_EXCEPTIONS = True

    USER = 'toobib' # paste here the same value of USER declared in the Makefile
    PWD = 'jesuispsql' # paste here the same value of PWD declared in the Makefile
    DB = 'toobib' # paste here the same value of DB declared in the Makefile
    EXTERNAL_DIRECTORY_SCHEMA = 'external' # paste here the same value of SCHEMA declared in the Makefile
    
    POSTGRES = {
        'user': USER,
        'pwd': PWD,
        'db': DB,
        'host': 'localhost',
        'port': '5432',
    
    }
    SQLALCHEMY_DATABASE_URI = 'postgresql://%(user)s:%(pwd)s@%(host)s:%(port)s/%(db)s' % POSTGRES
    PROPAGATE_EXCEPTIONS = True
    
    SESSION_COOKIE_SECURE = True

class ProductionConfig(Config):
    pass

class DevConfig(Config):
    DEBUG = True
    SESSION_COOKIE_SECURE = False

class TestingConfig(Config):
    TESTING = True
    SESSION_COOKIE_SECURE = False
```

- be sure to be in the root folder and run this command
``` bash
  - sudo apt-get install python3-pip python-psycopg2 libpq-dev
  - pip3 install --upgrade setuptools
  - pip3 install --upgrade pip
  - pip3 install --editable .
  - pip3 install Flask
  - pip3 uninstall flask_jwt_extended (this command command is only necessary if your current version of flask_jwt_extended is <4.1)
  - pip3 install flask_jwt_extended
  - FLASK_APP=app_flask FLASK_RUN_PORT=5000 FLASK_ENV=development flask run
```

Distinct Categories
============

#### Professions
SELECT distinct profession from annuaire;
 Pharmacien
 Ostéopathe
 Audioprothésiste
 Oculariste
 Médecin
 Chiropracteur
 Sage-Femme
 Psychothérapeute
 Orthoptiste
 Pédicure-Podologue
 Orthopédiste-Orthésiste
 Technicien de laboratoire médical
 Psychologue
 Masseur-Kinésithérapeute
 Psychomotricien
 Podo-Orthésiste
 Orthoprothésiste
 Opticien-Lunetier
 Manipulateur ERM
 Diététicien
 Assistant dentaire
 Orthophoniste
 Assistant de service social
 Epithésiste
 Chirurgien-Dentiste
 Infirmier
 Ergothérapeute
(27 rows)


#### Savoir faire
SELECT distinct savoirfaire from annuaire;
Maladies infectieuses et tropicales
 Médecine Générale
 Chirurgie infantile
 Obstétrique
 Allergologie
 Chirurgie générale
 Neuro-chirurgie
 Chirurgie thoracique et cardio-vasculaire
 Gynéco-obstétrique et Gynéco médicale option Gynéco-obst
 Médecine interne et immunologie clinique
 Anatomie et cytologie pathologiques
 CHIRURGIE ORALE
 Ophtalmologie
 Chirurgie urologique
 Gynécologie médicale et obstétrique
 Cardiologie et maladies vasculaires
 Hématologie
 Radio-thérapie
 Gynécologie médicale
 Médecine d'urgence
 Neuro-psychiatrie
 Médecine intensive-réanimation
 Anesthesie-réanimation
 Santé publique et médecine sociale
 Chirurgie maxillo-faciale et stomatologie
 Gastro-entérologie et hépatologie
 Gynéco-obstétrique et Gynéco médicale option Gynéco-médicale
 Médecine interne
 Spécialiste en Médecine Générale
 Chirurgie maxillo-faciale
 Biologie médicale
 Endocrinologie et métabolisme
 Hématologie (option Onco-hématologie)
 Médecine vasculaire
 Médecine du travail
 Oncologie option médicale
 Radio-diagnostic
 Qualification PAC
 Qualifié en Médecine Générale
 Pneumologie
 Chirurgie viscérale et digestive
 Oncologie option radiothérapie
 Rhumatologie
 Médecine légale et expertises médicales
 Chirurgie plastique reconstructrice et esthétique
 Psychiatrie
 Urologie
 Hématologie (option Maladie du sang)
 Oto-rhino-laryngologie
 Hématologie (réforme 2017)
 Médecine Bucco-Dentaire
 Chirurgie maxillo-faciale (réforme 2017)
 O.R.L et chirurgie cervico faciale
 Chirurgie Orale
 Chirurgie orthopédique et traumatologie
 Psychiatrie option enfant & adolescent
 Gériatrie
 Génétique médicale
 Orthopédie dento-faciale
 Neurologie
 Chirurgie vasculaire
 Médecine nucléaire
 Recherche médicale
 Endocrinologie, diabétologie, nutrition
 Pédiatrie
 Oncologie (option onco-hématologie)
 Gynécologie-obstétrique
 Médecine physique et réadaptation
 Dermatologie et vénéréologie
 Néphrologie
 Stomatologie
(72 rows)


Interesting resources
============
#### Make
- https://software-carpentry.org/lessons/
  - http://swcarpentry.github.io/make-novice/
- http://www.leeladharan.com/sqlalchemy-query-with-or-and-like-common-filters

#### Full Text Search In Postgres
> The pg_trgm module provides functions and operators for determining the similarity of ASCII alphanumeric text based on trigram matching, as well as index operator classes that support fast searching for similar strings.
> In choosing which index type to use, GiST or GIN, consider these performance differences:
- GIN index lookups are about three times faster than GiST
- GIN indexes take about three times longer to build than GiST
- GIN indexes are moderately slower to update than GiST indexes, but about 10 times slower if fast-update support was disabled [...]
- GIN indexes are two-to-three times larger than GiST indexes

- https://niallburkley.com/blog/index-columns-for-like-in-postgres/
- https://www.postgresql.org/docs/9.1/textsearch-indexes.html

##### Links

1. [annuaire.sante.fr](https://annuaire.sante.fr/web/site-pro)
- [Annuaire SantéNouvelles extractions en libre accès](https://esante.gouv.fr/sites/default/files/media_entity/documents/ANS_Description-des-nouvelles-extractions-en-libre-acces_V1.2.pdf)
- [Dossier des Spécifications Fonctionnelles et Techniques](https://esante.gouv.fr/sites/default/files/media_entity/documents/Annuaire_sante_fr_DSFT_Extractions_donnees_libre%20acc%C3%A8s_V2.2.2.pdf)
- [Extraction annuaire - Quality check](https://pad.interhop.org/s/HyeeUgt1v#)

2. [Annuaire santé de la Cnam](https://www.data.gouv.fr/fr/datasets/annuaire-sante-de-la-cnam/#_)
- [Open Data for Health](https://public.opendatasoft.com/explore/?sort=modified&q=annuaire&refine.theme=Sant%C3%A9)
>>>>>>> origin/fusionnerWithServeurData
  - [Nomenclature Professions du domaine de la santé](https://public.opendatasoft.com/explore/dataset/nomenclature-professions-medecine/table/)
  - [Annuaire et localisation des professionnels de santé ](https://public.opendatasoft.com/explore/dataset/annuaire-des-professionnels-de-sante/table/)
  - [Annuaire Santé : Liste, localisation et tarifs des professionnels de santé ](https://public.opendatasoft.com/explore/dataset/medecins/table/)

3. [Base officielle des codes postaux ](https://datanova.laposte.fr/explore/dataset/laposte_hexasmal/information/?disjunctive.code_commune_insee&disjunctive.nom_de_la_commune&disjunctive.code_postal&disjunctive.ligne_5)

4. [FINESS : Liste des établissements du domaine sanitaire et social.](https://www.data.gouv.fr/fr/datasets/finess-extraction-du-fichier-des-etablissements/#_)
- DOC Structure: Établissement (ET) Structure & Géolocalisation
- DOC Structure: Établissement (ET) Structure & Géolocalisation
- [Definitions](http://finess.sante.gouv.fr/fininter/jsp/definitions.do?codeDefinition=2)
- [Search](http://finess.sante.gouv.fr/fininter/jsp/actionRechercheSimple.do)
- [Definition : RPPS -  AM / FINESS](https://www.ameli.fr/fileadmin/user_upload/documents/memo_RPPS.pdf)

5. [Base Sirene v3](https://public.opendatasoft.com/explore/dataset/sirene_v3/information/?disjunctive.libellecommuneetablissement&disjunctive.etatadministratifetablissement&disjunctive.sectionetablissement&disjunctive.naturejuridiqueunitelegale&sort=datederniertraitementetablissement&q=44261444200220)
- [Numéro SIREN, numéro SIRET, quelle différence ?](https://www.economie.gouv.fr/entreprises/numeros-siren-siret)