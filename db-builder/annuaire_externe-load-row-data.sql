/*********************************************************************************
#
#
#     Licence
#
#
#
********************************************************************************/

/************************

 ####### #######  ####### ######     #    ###### 
    #    #     #  #     # #     #    #    #     #
    #    #     #  #     # #     #    #    #     #
    #    #     #  #     # ######     #    ###### 
    #    #     #  #     # #     #    #    #     #
    #    #     #  #     # #     #    #    #     #
    #    #######  ####### ######     #    ###### 

last revised: 13-Jul-Aug-2020

Authors:  Adrien PARROT

*************************/


/************************

Profesionnels de sante Libre Access

************************/

TRUNCATE TABLE ps_libreacces_personne_activite CASCADE;
DROP INDEX IF EXISTS idx_personne_activite_id_nat_pp;
TRUNCATE TABLE ps_libreacces_dipl_autexerc CASCADE;
TRUNCATE TABLE ps_libreacces_savoirfaire CASCADE;

\copy ps_libreacces_personne_activite FROM 'download/PS_LibreAcces_Personne_activite.csv' WITH DELIMITER '|' CSV HEADER ;
\copy ps_libreacces_dipl_autexerc FROM 'download/PS_LibreAcces_Dipl_AutExerc.csv' WITH DELIMITER '|' CSV HEADER ;
\copy ps_libreacces_savoirfaire FROM 'download/PS_LibreAcces_SavoirFaire.csv' WITH DELIMITER '|' CSV HEADER ;

/************************

Carte CPS

************************/

TRUNCATE TABLE porteurs_CPS_CPF CASCADE;
DROP INDEX IF EXISTS idx_porteurs_CPS_CPF_id_nat_pp;

\copy porteurs_CPS_CPF FROM 'download/Porteurs_CPS_CPF.csv' WITH DELIMITER '|' CSV HEADER ;

/************************

Boite aux lettres (BAT) MSSante

************************/

TRUNCATE TABLE extraction_correspondance_mssante CASCADE;

\copy extraction_correspondance_mssante FROM 'download/Extraction_Correspondance_MSSante.csv' WITH DELIMITER '|' CSV HEADER ;


/************************

Create Index

************************/

CREATE INDEX idx_personne_activite_id_nat_pp  ON  ps_libreacces_personne_activite (identification_nationale_PP ASC);
CLUSTER ps_libreacces_personne_activite  USING idx_personne_activite_id_nat_pp ;

CREATE INDEX idx_porteurs_CPS_CPF_id_nat_pp  ON  porteurs_CPS_CPF (identification_nationale_PP ASC);
CLUSTER porteurs_CPS_CPF USING idx_porteurs_CPS_CPF_id_nat_pp;
