/*********************************************************************************
#
#
#     Licence
#
#
#
 ********************************************************************************/

/************************

####### #######  ####### ######     #    ###### 
#    #     #  #     # #     #    #    #     #
#    #     #  #     # #     #    #    #     #
#    #     #  #     # ######     #    ###### 
#    #     #  #     # #     #    #    #     #
#    #     #  #     # #     #    #    #     #
#    #######  ####### ######     #    ###### 

last revised: 13-Jul-Aug-2020

Authors:  Adrien PARROT

 *************************/

/************************

Profesionnels de sante Libre Access

 ************************/

DROP TABLE IF EXISTS annuaire;

CREATE TABLE annuaire (
	annuaire_id 					INTEGER 	NOT NULL,
	insert_datetime					TIMESTAMP 	NOT NULL,
	update_datetime					TIMESTAMP	NULL,	
	search 		 				TEXT		NULL,
	type_identifiant_PP 				TEXT		NULL,
	identifiant_PP 				 	TEXT		NULL,
	identification_nationale_PP 			TEXT		NULL,
	code_civilite_exercice 				TEXT		NULL,
	libelle_civilite_exercice 			TEXT		NULL,
	code_civilite 					TEXT		NULL,
	libelle_civilite 				TEXT		NULL,
	nom 						TEXT		NULL,
	nom_normalise 					TEXT		NULL,
	prenom 						TEXT		NULL,
	prenom_normalise 				TEXT		NULL,
	code_profession 				TEXT		NULL,
	libelle_profession 				TEXT		NULL,
	libelle_profession_normalise 			TEXT		NULL,
	code_categorie_professionnelle 			TEXT		NULL,
	libelle_categorie_professionnelle 		TEXT		NULL,
	code_type_savoirfaire 				TEXT		NULL,
	libelle_type_savoirfaire  			TEXT		NULL,
	code_savoirfaire 				TEXT		NULL,
	libelle_savoirfaire  				TEXT		NULL,
	libelle_savoirfaire_normalise  			TEXT		NULL,
	code_mode_exercice 				TEXT		NULL,
	libelle_mode_exercice 				TEXT		NULL,
	siret 						TEXT		NULL,
	siren						TEXT		NULL,
	finess 						TEXT		NULL,
	finess_etablissement_juridique 			TEXT		NULL,
	id_technique_structure 				TEXT		NULL,
	raison_sociale 					TEXT		NULL,
	enseigne_com  					TEXT		NULL,
	complement_destinataire  			TEXT		NULL,
	complement_point_geographique  			TEXT		NULL,
	numero_voie           				TEXT		NULL,
	indice_repetition_voie    			TEXT		NULL,
	code_type_de_voie 				TEXT		NULL,
	libelle_type_de_voie				TEXT		NULL,
	libelle_voie					TEXT		NULL,
	mention_distribution				TEXT		NULL,
	bureau_cedex					TEXT		NULL,
	code_postal					TEXT		NULL,
	code_commune					TEXT		NULL,
	libelle_commune					TEXT		NULL,
	libelle_commune_normalise			TEXT		NULL,
	code_pays					TEXT		NULL,
	libelle_pays					TEXT		NULL,
	telephone					TEXT		NULL,
	telephone_2					TEXT		NULL,
	telecopie					TEXT		NULL,
	email						TEXT		NULL,
	code_departement				TEXT		NULL,
	libelle_departement				TEXT		NULL,
	ancien_id_structure				TEXT		NULL,
	autorite_enregistrement				TEXT		NULL,
	code_secteur_activite				TEXT		NULL,
	libelle_secteur_activite			TEXT		NULL,
	code_section_tableau_pharmaciens		TEXT		NULL,
	libelle_section_tableau_pharmaciens		TEXT		NULL,
	code_type_carte 				TEXT		NULL,
	libelle_carte   				TEXT		NULL,
	numero_carte 	  				TEXT		NULL,
	id_national_carte  				TEXT		NULL,
	debut_valid_date   				TEXT		NULL,
	fin_valid_date   				TEXT		NULL,
	opposition_date   				TEXT		NULL,
	miseajour_date   				TEXT		NULL
)
;


CREATE TABLE ps_libreacces_personne_activite (
	type_identifiant_PP 				TEXT	NULL,
	identifiant_PP 				 	TEXT	NULL,
	identification_nationale_PP 			TEXT	NULL,
	code_civilite_exercice 				TEXT	NULL,
	libelle_civilite_exercice 			TEXT	NULL,
	code_civilite 					TEXT	NULL,
	libelle_civilite 				TEXT	NULL,
	nom 						TEXT	NULL,
	prenom 						TEXT	NULL,
	code_profession 				TEXT	NULL,
	libelle_profession 				TEXT	NULL,
	code_categorie_professionnelle 			TEXT	NULL,
	libelle_categorie_professionnelle 		TEXT	NULL,
	code_type_savoirfaire 				TEXT	NULL,
	libelle_type_savoirfaire  			TEXT	NULL,
	code_savoirfaire 				TEXT	NULL,
	libelle_savoirfaire  				TEXT	NULL,
	code_mode_exercice 				TEXT	NULL,
	libelle_mode_exercice 				TEXT	NULL,
	siret 						TEXT	NULL,
	siren						TEXT	NULL,
	finess 						TEXT	NULL,
	finess_etablissement_juridique 			TEXT	NULL,
	id_technique_structure 				TEXT	NULL,
	raison_sociale 					TEXT	NULL,
	enseigne_com  					TEXT	NULL,
	complement_destinataire  			TEXT	NULL,
	complement_point_geographique  			TEXT	NULL,
	numero_voie           				TEXT	NULL,
	indice_repetition_voie    			TEXT	NULL,
	code_type_de_voie 				TEXT	NULL,
	libelle_type_de_voie				TEXT	NULL,
	libelle_voie					TEXT	NULL,
	mention_distribution				TEXT	NULL,
	bureau_cedex					TEXT	NULL,
	code_postal					TEXT	NULL,
	code_commune					TEXT	NULL,
	libelle_commune					TEXT	NULL,
	code_pays					TEXT	NULL,
	libelle_pays					TEXT	NULL,
	telephone					TEXT	NULL,
	telephone_2					TEXT	NULL,
	telecopie					TEXT	NULL,
	email						TEXT	NULL,
	code_departement				TEXT	NULL,
	libelle_departement				TEXT	NULL,
	ancien_id_structure				TEXT	NULL,
	autorite_enregistrement				TEXT	NULL,
	code_secteur_activite				TEXT	NULL,
	libelle_secteur_activite			TEXT	NULL,
	code_section_tableau_pharmaciens		TEXT	NULL,
	libelle_section_tableau_pharmaciens		TEXT	NULL
)
;


CREATE TABLE ps_libreacces_dipl_autexerc (
	type_identifiant_PP 				TEXT	NULL,
	identifiant_PP 				 	TEXT	NULL,
	identification_nationale_PP 			TEXT	NULL,
	nom 						TEXT	NULL,
	prenom 						TEXT	NULL,
	code_type_diplome 				TEXT	NULL,
	libelle_type_diplome  				TEXT	NULL,
	code_diplome 					TEXT	NULL,
	libelle_diplome  				TEXT	NULL,
	code_type_autorisation 				TEXT	NULL,
	libelle_type_autorisation 			TEXT	NULL,
	code_discipline_autorisation 			TEXT	NULL,
	libelle_discipline_autorisation 		TEXT	NULL
)
;

CREATE TABLE ps_libreacces_savoirfaire (
	type_identifiant_PP 				TEXT	NULL,
	identifiant_PP 				 	TEXT	NULL,
	identification_nationale_PP 			TEXT	NULL,
	nom 						TEXT	NULL,
	prenom 						TEXT	NULL,
	code_profession 				TEXT	NULL,
	libelle_profession 				TEXT	NULL,
	code_categorie_professionnelle 			TEXT	NULL,
	libelle_categorie_professionnelle 		TEXT	NULL,
	code_type_savoirfaire 				TEXT	NULL,
	libelle_type_savoirfaire  			TEXT	NULL,
	code_savoirfaire 				TEXT	NULL,
	libelle_savoirfaire  				TEXT	NULL
)
;

/************************

Carte CPS

 ************************/

CREATE TABLE porteurs_CPS_CPF (
	identifiant_PP 				 	TEXT	NULL,
	identification_nationale_PP 			TEXT	NULL,
	type_identifiant_PP 				TEXT	NULL,
	nom 						TEXT	NULL,
	prenom 						TEXT	NULL,
	code_profession 				TEXT	NULL,
	libelle_profession 				TEXT	NULL,
	code_categorie_professionnelle 			TEXT	NULL,
	libelle_categorie_professionnelle 		TEXT	NULL,
	code_type_carte 				TEXT	NULL,
	libelle_carte   				TEXT	NULL,
	numero_carte 	  				TEXT	NULL,
	id_national_carte  				TEXT	NULL,
	debut_valid_date   				TEXT	NULL,
	fin_valid_date   				TEXT	NULL,
	opposition_date   				TEXT	NULL,
	miseajour_date   				TEXT	NULL
)
;

/************************

Boite aux lettres (BAT) MSSante

 ************************/

CREATE TABLE extraction_correspondance_mssante (
	type_bal 					TEXT	NULL,
	addresse_bal 					TEXT	NULL,
	type_identifiant_PP 				TEXT	NULL,
	identifiant_PP 				 	TEXT	NULL,
	identification_nationale_PP 			TEXT	NULL,
	type_id_structure 				TEXT	NULL,
	id_structure 					TEXT	NULL,
	service_rattachement 				TEXT	NULL,
	civilite 					TEXT	NULL,
	nom 						TEXT	NULL,
	prenom 						TEXT	NULL,
	cat_profession 					TEXT	NULL,
	libelle_cat_profession 				TEXT	NULL,
	code_profession 				TEXT	NULL,
	libelle_profession 				TEXT	NULL,
	code_savoirfaire 				TEXT	NULL,
	libelle_savoirfaire  				TEXT	NULL,
	dematerialisation  				TEXT	NULL,
	raison_sociale  				TEXT	NULL,
	enseigne_com  					TEXT	NULL,
	L2COMPLEMENTLOCALISATION_structure              TEXT	NULL,
	L3COMPLEMENTDISTRIBUTION_structure              TEXT	NULL,
	L4NUMEROVOIE_structure                          TEXT	NULL,
	L4COMPLEMENTNUMEROVOIE_structure                TEXT	NULL,
	NL4TYPEVOIE_structure                           TEXT	NULL,
	L4LIBELLEVOIE_structure                         TEXT	NULL,
	L5LIEUDITMENTION_structure                      TEXT	NULL,
	L6LIGNEACHEMINEMENT_structure                   TEXT	NULL,
	code_postal_structure                           TEXT	NULL,
	departement_structure                           TEXT	NULL,
	pays_structure 					TEXT 	NULL
)
;
