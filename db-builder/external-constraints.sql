/*********************************************************************************
#
#
#     Licence
#
#
#
 ********************************************************************************/

/************************

####### #######  ####### ######     #    ######
   #    #     #  #     # #     #    #    #     #
   #    #     #  #     # #     #    #    #     #
   #    #     #  #     # ######     #    ######
   #    #     #  #     # #     #    #    #     #
   #    #     #  #     # #     #    #    #     #
   #    #######  ####### ######     #    ######

last revised: 03-April-2021

Authors:  Adrien PARROT
Authors:  Quentin PARROT

 *************************/

-- -------------
-- -- profession
-- -------------
-- ALTER TABLE profession ADD CONSTRAINT xpk_profession_table PRIMARY KEY (profession_id);
-- CREATE SEQUENCE profession_id_seq OWNED BY profession.profession_id;
-- ALTER TABLE profession ALTER COLUMN profession_id SET DEFAULT nextval('profession_id_seq');
-- 
-- ALTER TABLE profession ALTER COLUMN insert_datetime SET DEFAULT now();
-- 
-- ----------------------------
-- -- categorie_professionnelle
-- ----------------------------
-- ALTER TABLE categorie_professionnelle ADD CONSTRAINT xpk_categorie_professionnelle_table PRIMARY KEY (categorie_professionnelle_id);
-- CREATE SEQUENCE categorie_professionnelle_id_seq OWNED BY categorie_professionnelle.categorie_professionnelle_id;
-- ALTER TABLE categorie_professionnelle ALTER COLUMN categorie_professionnelle_id SET DEFAULT nextval('categorie_professionnelle_id_seq');
-- 
-- ALTER TABLE categorie_professionnelle ALTER COLUMN insert_datetime SET DEFAULT now();
-- 
-- -------------
-- -- savoir_faire
-- -------------
-- ALTER TABLE savoir_faire ADD CONSTRAINT xpk_savoir_faire_table PRIMARY KEY (savoir_faire_id);
-- CREATE SEQUENCE savoir_faire_id_seq OWNED BY savoir_faire.savoir_faire_id;
-- ALTER TABLE savoir_faire ALTER COLUMN savoir_faire_id SET DEFAULT nextval('savoir_faire_id_seq');
-- 
-- ALTER TABLE savoir_faire ALTER COLUMN insert_datetime SET DEFAULT now();
-- 
-- 
-------------
-- concept
-------------
ALTER TABLE concept ADD CONSTRAINT xpk_concept PRIMARY KEY (concept_id);
CREATE SEQUENCE concept_id_seq OWNED BY concept.concept_id;
ALTER TABLE concept ALTER COLUMN concept_id SET DEFAULT nextval('concept_id_seq');

ALTER TABLE concept ALTER COLUMN valid_start_datetime SET DEFAULT now();
ALTER TABLE concept ALTER COLUMN insert_datetime SET DEFAULT now();


ALTER TABLE concept ADD CONSTRAINT chk_c_concept_name CHECK (concept_name <> '');
ALTER TABLE concept ADD CONSTRAINT chk_c_concept_code CHECK (concept_code <> '');

ALTER TABLE concept ADD CONSTRAINT chk_c_invalid_reason CHECK (COALESCE(invalid_reason,'D') in ('D','U'));

-- https://www.postgresqltutorial.com/postgresql-unique-constraint/
CREATE UNIQUE INDEX  concept_concept_name  ON concept (concept_code, vocabulary_id);
ALTER TABLE concept ADD CONSTRAINT unique_concept_name UNIQUE USING INDEX concept_concept_name;

-- -----------
-- -- recherche
-- -----------
-- ALTER TABLE recherche ADD CONSTRAINT xpk_recherche_table PRIMARY KEY (rech_id);
-- CREATE SEQUENCE recherche_id_seq OWNED BY recherche.rech_id;
-- ALTER TABLE recherche ALTER COLUMN rech_id SET DEFAULT nextval('recherche_id_seq');
-- 
-- ALTER TABLE recherche ALTER COLUMN insert_datetime SET DEFAULT now();
-- -- ALTER TABLE recherche ALTER COLUMN update_datetime SET DEFAULT NULL;
-- 
-- ALTER TABLE recherche ADD FOREIGN KEY (profession_id) REFERENCES profession (profession_id);
-- ALTER TABLE recherche ADD FOREIGN KEY (categorie_professionnelle_id) REFERENCES categorie_professionnelle (categorie_professionnelle_id);
-- ALTER TABLE recherche ADD FOREIGN KEY (savoir_faire_id) REFERENCES savoir_faire (savoir_faire_id);

-------------
-- professionnel
-------------
ALTER TABLE professionnel ADD CONSTRAINT xpk_professionnel_table PRIMARY KEY (pro_id);
CREATE SEQUENCE professionnel_id_seq OWNED BY professionnel.pro_id;
ALTER TABLE professionnel ALTER COLUMN pro_id SET DEFAULT nextval('professionnel_id_seq');

ALTER TABLE professionnel ALTER COLUMN insert_datetime SET DEFAULT now();

-------------
-- etablissement
-------------
ALTER TABLE etablissement ADD CONSTRAINT xpk_etablissement_table PRIMARY KEY (etab_id);
CREATE SEQUENCE etablissement_id_seq OWNED BY etablissement.etab_id;
ALTER TABLE etablissement ALTER COLUMN etab_id SET DEFAULT nextval('etablissement_id_seq');
-- ALTER TABLE etablissement ADD FOREIGN KEY (pro_id) REFERENCES professionnel (pro_id); 
-- could not put a foreign because inpp is not unique in professionnel table

ALTER TABLE etablissement ALTER COLUMN insert_datetime SET DEFAULT now();

-------------
-- cps
-------------
ALTER TABLE cps ADD CONSTRAINT xpk_cps_table PRIMARY KEY (cps_id);
CREATE SEQUENCE cps_id_seq OWNED BY cps.cps_id;
ALTER TABLE cps ALTER COLUMN cps_id SET DEFAULT nextval('cps_id_seq');
-- ALTER TABLE cps ADD FOREIGN KEY (inpp) REFERENCES professionnel (inpp);
-- could not put a foreign because inpp is not unique in professionnel table

ALTER TABLE cps ALTER COLUMN insert_datetime SET DEFAULT now();
-------------
-- mss
-------------
ALTER TABLE mss ADD CONSTRAINT xpk_mss_table PRIMARY KEY (mss_id);
CREATE SEQUENCE mss_id_seq OWNED BY mss.mss_id;
ALTER TABLE mss ALTER COLUMN mss_id SET DEFAULT nextval('mss_id_seq');
-- ALTER TABLE mss ADD FOREIGN KEY (inpp) REFERENCES professionnel (inpp);
-- could not put a foreign because inpp is not unique in professionnel table

ALTER TABLE mss ALTER COLUMN insert_datetime SET DEFAULT now();
