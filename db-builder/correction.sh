echo "Dipl_Aut_Exerc_ in progress..."
cut -d '|' -f 1-13 download/PS_LibreAcces_Dipl_AutExerc_* > download/PS_LibreAcces_Dipl_AutExerc.csv
echo "SavoirFaire_ in progress..."
cut -d '|' -f 1-13 download/PS_LibreAcces_SavoirFaire_* > download/PS_LibreAcces_SavoirFaire.csv
echo "Personne_activite_ in progress..."
cut -d '|' -f 1-52 download/PS_LibreAcces_Personne_activite_* > download/PS_LibreAcces_Personne_activite.csv
echo "Porteurs_CPS_CSF_ in progress..."
cut -d '|' -f 1-17 download/Porteurs_CPS_CPF_* > download/Porteurs_CPS_CPF.csv
echo "Extraction_Correspondance_MSSante_ in progress..."
sed ':a;N;$!ba;s/\n|/|/g' -i download/Extraction_Correspondance_MSSante_*
sed 's/"LE CLOS À FROMENT - LA GLACERIE|/"LE CLOS À FROMENT - LA GLACERIE"|/g' -i download/Extraction_Correspondance_MSSante_*
cut -d '|' -f 1-31  download/Extraction_Correspondance_MSSante_* > download/Extraction_Correspondance_MSSante.csv
echo "finess_struct in progress..."
grep --binary-files=text -e '^structureet;' 'download/4445601e-a487-4eeb-990b-53c67720bb81' > download/finess_s.csv
cut -d ';' -f 2-32 download/finess_s.csv >  download/finess_struct.csv
echo "finess_geo in progress..."
grep --binary-files=text -e '^geolocalisation;' 'download/4445601e-a487-4eeb-990b-53c67720bb81' > download/finess_g.csv
cut -d ';' -f 2-6 download/finess_g.csv > download/finess_geo.csv
