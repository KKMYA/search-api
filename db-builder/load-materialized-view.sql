/*********************************************************************************
#
#
#     Licence
#
#
#
 ********************************************************************************/

/************************

####### #######  ####### ######     #    ######
   #    #     #  #     # #     #    #    #     #
   #    #     #  #     # #     #    #    #     #
   #    #     #  #     # ######     #    ######
   #    #     #  #     # #     #    #    #     #
   #    #     #  #     # #     #    #    #     #
   #    #######  ####### ######     #    ######

last revised: 03-April-2021

Authors:  Adrien PARROT
Authors:  Quentin PARROT

 *************************/

/************************

DROP index
TRUNCATE TABLE

*************************/

DROP MATERIALIZED VIEW IF EXISTS recherche;

/************************

FILL API TABLES

*************************/


--- recherche
--- CREATE MATERIAZED VIEWS AS avec index
CREATE MATERIALIZED VIEW recherche AS
	SELECT  p.identification_nationale_pp 											AS inpp
		, p.nom														AS nom
		, coalesce(multi_replace(lower(public.unaccent(p.nom)), '{" ": "", "-":""}'::jsonb), '')			AS nom_normalise
		, p.prenom													AS prenom
		, coalesce(multi_replace(lower(public.unaccent(p.prenom)), '{" ": "", "-":""}'::jsonb), '')			AS prenom_normalise
		, p.libelle_profession												AS profession
		, coalesce(multi_replace(lower(public.unaccent(p.libelle_profession)), '{" ": "", "-":""}'::jsonb), '')		AS profession_normalise
		, p.libelle_savoirfaire												AS savoir_faire
		, coalesce(multi_replace(lower(public.unaccent(p.libelle_savoirfaire)), '{" ": "", "-":""}'::jsonb), '')	AS savoir_faire_normalise
		, p.libelle_commune												AS commune
		, coalesce(multi_replace(lower(public.unaccent(p.libelle_commune)), '{" ": "", "-":""}'::jsonb), '')		AS commune_normalise
	FROM ps_libreacces_personne_activite p;



/************************

Create Index

*************************/

CREATE INDEX idx_recherche_inpp ON recherche  (inpp ASC);
CREATE INDEX idx_recherche_prenom_normalise ON recherche USING GIN (prenom_normalise public.gin_trgm_ops);
CREATE INDEX idx_recherche_nom_normalise ON recherche USING GIN (nom_normalise public.gin_trgm_ops);
CREATE INDEX idx_recherche_profession_normalise ON recherche  USING GIN (profession_normalise public.gin_trgm_ops);
CREATE INDEX idx_recherche_savoir_faire_normalise ON recherche USING GIN (savoir_faire_normalise public.gin_trgm_ops);
CREATE INDEX idx_recherche_commune_normalise ON recherche USING GIN (commune_normalise public.gin_trgm_ops);
-- CREATE INDEX idx_recherche_profession ON recherche  (libelle_profession ASC);
