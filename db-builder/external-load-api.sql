/*********************************************************************************
#
#
#     Licence
#
#
#
 ********************************************************************************/

/************************

####### #######  ####### ######     #    ######
   #    #     #  #     # #     #    #    #     #
   #    #     #  #     # #     #    #    #     #
   #    #     #  #     # ######     #    ######
   #    #     #  #     # #     #    #    #     #
   #    #     #  #     # #     #    #    #     #
   #    #######  ####### ######     #    ######

last revised: 04-Feb-2021

Authors:  Adrien PARROT
Authors:  Quentin PARROT

 *************************/

/************************

DROP index
TRUNCATE TABLE

*************************/

DROP INDEX IF EXISTS idx_professionnel_inpp; 
DROP INDEX IF EXISTS idx_etablissement_inpp;
DROP INDEX IF EXISTS idx_cps_inpp;
DROP INDEX IF EXISTS idx_mss_inpp;

TRUNCATE TABLE professionnel CASCADE;
TRUNCATE TABLE etablissement CASCADE;
TRUNCATE TABLE cps CASCADE;
TRUNCATE TABLE mss CASCADE;

/************************

FILL API TABLES

*************************/

-- profession
INSERT INTO concept (
	concept_name
	, concept_code                                 
	, domain_id
	, vocabulary_id
        , t_language_id
)
SELECT DISTINCT p.libelle_profession 				
	, p.code_profession 					
	, 'PROFESSIONNEL'
	, 'PROFESSION'
	, 'FR'
FROM ps_libreacces_personne_activite p
WHERE p.libelle_profession IS NOT NULL
;

--- categorie_professionnelle
INSERT INTO concept (
	concept_name
	, concept_code                                 
	, domain_id
	, vocabulary_id
        , t_language_id
)
SELECT DISTINCT  p.libelle_categorie_professionnelle 		
	, p.code_categorie_professionnelle 			
	, 'PROFESSIONNEL'
	, 'CATEGORIE_PROFESSIONNELLE'
	, 'FR'
FROM ps_libreacces_personne_activite p
WHERE p.libelle_categorie_professionnelle IS NOT NULL
;

--- savoir_faire
INSERT INTO concept (
	concept_name
	, concept_code                                 
	, domain_id
	, vocabulary_id
        , t_language_id
)
SELECT DISTINCT p.libelle_savoirfaire 		
	, p.code_savoirfaire 			
	, 'PROFESSIONNEL'
	, 'SAVOIR_FAIRE'
	, 'FR'
FROM ps_libreacces_personne_activite p
WHERE p.libelle_savoirfaire IS NOT NULL
;

CREATE INDEX idx_concept_concept_code ON concept (concept_code ASC);
CREATE INDEX idx_concept_vocabulary_id ON concept (vocabulary_id ASC);
CREATE INDEX idx_concept_domain_id ON concept (domain_id ASC);
-- CREATE UNIQUE INDEX idx_concept_concept_name ON concept (concept_name ASC);




-- professionnel
INSERT INTO professionnel (
        inpp                                            
        , civilite                                        
        , civilite_exercice                               
        , nom                                             
        , nom_normalise                                   
        , prenom                                          
        , prenom_normalise                                
        , profession_id                                   
        , profession                                      
        , categorie_professionnelle_id                    
        , categorie_professionnelle            
        , savoir_faire_id                                 
        , savoir_faire                                    
)
SELECT distinct p.identification_nationale_pp 			
	, p.libelle_civilite 					
	, p.libelle_civilite_exercice 			
	, p.nom 						
	, multi_replace(lower(public.unaccent(p.nom)), '{" ": "", "-":""}'::jsonb)			
	, p.prenom 						
	, multi_replace(lower(public.unaccent(p.prenom)), '{" ": "", "-":""}'::jsonb)	
	, prof.concept_id
	, p.libelle_profession	
	, cp.concept_id
	, p.libelle_categorie_professionnelle
	, sf.concept_id
	, p.libelle_savoirfaire 				
FROM ps_libreacces_personne_activite p
	LEFT JOIN concept prof ON (p.code_profession = prof.concept_code) AND prof.vocabulary_id = 'PROFESSION'
	LEFT JOIN concept cp ON (p.code_categorie_professionnelle = cp.concept_code) AND cp.vocabulary_id = 'CATEGORIE_PROFESSIONNELLE'
	LEFT JOIN concept sf ON (p.code_savoirfaire = sf.concept_code) AND sf.vocabulary_id = 'SAVOIR_FAIRE'
;


-- etablissement
INSERT INTO etablissement (
        inpp                                            
        , mode_exercice                                   
        , siret                                           
        , siren                                           
        , finess                                          
        , finess_etablissement_juridique                  
        , numero_voie                                     
        , indice_repetition_voie                          
        , type_de_voie                                    
        , voie                                            
        , mention_distribution                            
        , bureau_cedex                                    
        , code_postal                                     
        , code_commune                                    
        , commune                                 
        , commune_normalise                       
        , telephone                                       
        , telephone_2                                     
        , telecopie                                       
        , email                                           
        , gps_finess                                      
        , gps_code_commune                                
        , gps_code_postal                                
 
)
SELECT DISTINCT p.identification_nationale_pp
	,  p.libelle_mode_exercice
	,  p.siret
	,  p.siren
	,  p.finess
	,  p.finess_etablissement_juridique
	,  p.numero_voie
	,  p.indice_repetition_voie
	,  p.libelle_type_de_voie
	,  p.libelle_voie
	,  p.mention_distribution
	,  p.bureau_cedex
	,  p.code_postal
	,  p.code_commune
	,  p.libelle_commune
	,  multi_replace(lower(public.unaccent(p.libelle_commune)), '{" ": "", "-":""}'::jsonb) 	
	,  p.telephone
	,  p.telephone_2
	,  p.telecopie
	,  p.email
	, fg.coord_x || ', ' || fg.coord_y
        , poste_cm.gps                                
        , poste_cp.gps                                
FROM ps_libreacces_personne_activite p
LEFT JOIN finess_geo fg ON (p.finess = fg.finess_et) 				-- finess_et is unique in finess_geo table
LEFT JOIN laposte_commune poste_cm ON (p.code_commune = poste_cm.code_commune_insee)
LEFT JOIN laposte_postal poste_cp ON (p.code_postal = poste_cp.code_postal)

;

-- cps
INSERT INTO cps (
        inpp                                            
        , type_carte                                      
        , numero_carte                                    
        , id_national_carte                               
        , debut_valid_date                                
        , fin_valid_date                                  
        , opposition_date                                 
        , miseajour_date                                  
)
SELECT DISTINCT identification_nationale_pp
	, code_type_carte
	, numero_carte
	, id_national_carte
	, debut_valid_date
	, fin_valid_date
	, opposition_date
	, miseajour_date
FROM porteurs_cps_cpf
; 



-- mss
INSERT INTO mss (
        inpp     
        , type_bal 
        , bal      
)
SELECT DISTINCT identification_nationale_pp
    , type_bal
    , addresse_bal
FROM extraction_correspondance_mssante
;


/************************

Create Index

*************************/

-- CREATE INDEX idx_recherche_profession ON recherche  (libelle_profession ASC);

CREATE INDEX idx_professionnel_inpp ON professionnel  (inpp ASC);
CREATE INDEX idx_etablissement_inpp ON etablissement  (inpp ASC);
CREATE INDEX idx_cps_inpp ON cps  (inpp ASC);
CREATE INDEX idx_mss_inpp ON mss  (inpp ASC);
