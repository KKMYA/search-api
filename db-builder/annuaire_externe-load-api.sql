/*********************************************************************************
#
#
#     Licence
#
#
#
********************************************************************************/

/************************

 ####### #######  ####### ######     #    ###### 
    #    #     #  #     # #     #    #    #     #
    #    #     #  #     # #     #    #    #     #
    #    #     #  #     # ######     #    ###### 
    #    #     #  #     # #     #    #    #     #
    #    #     #  #     # #     #    #    #     #
    #    #######  ####### ######     #    ###### 

last revised: 13-Jul-Aug-2020

Authors:  Adrien PARROT

*************************/

/************************

API keys

FROM ps_libreacces_personne_activite
- typeIdentifiantPP
- identifiantPP
- identificationNationalePP
- codeCiviliteExercice
- libelleCiviliteExercice
- codeCivilite
- libelleCivilite
- nom
- prenom
- codeProfession
- libelleProfession
- codeCategorieProfessionnelle
- libelleCategorieProfessionnelle
- codeTypeSavoirFaire
- libelleTypeSavoirFaire
- codeSavoirFaire
- libelleSavoirFaire
- codeModeExercice
- libelleModeExercice
- siret
- siren
- finess
- finessEtablissementJuridique
- identifiantTechniqueStructure
- raisonSociale
- enseigneCommerciale
- complementDestinataire
- complementGeographique
- numeroVoie
- indiceRepetitionVoie
- codeTypeVoie
- libelleTypeVoie
- libelleVoie
- mentionDistribution
- bureauCedex
- codePostal
- codeCommune
- libelleCommune
- codePays
- libellePays
- telephone
- telephoneDeux
- telecopie
- email
- codeDepartement
- libelleDepartement
- ancienIdentifiantStructure
- autoriteEnregistrement
- codeSecteurActivite
- libelleSecteurActivite
- codeSectionPharmacien
- libelleSectionPharmacien

FROM CPS                               
- codeCarte
- libelleCarte
- numeroCarte
- identifiantNationalCarte
- dateDebutValiditeCarte
- dateFinValiditeCarte
- dateOppositionCarte	
- dateMiseAJourCarte

}

*************************/

DROP INDEX IF EXISTS idx_annuaire_nom;
DROP INDEX IF EXISTS idx_annuaire_prenom;
DROP INDEX IF EXISTS idx_annuaire_commune;
DROP INDEX IF EXISTS idx_annuaire_profession;


TRUNCATE TABLE annuaire CASCADE;

DROP FUNCTION IF EXISTS quote_meta;
DROP FUNCTION IF EXISTS multi_replace;

CREATE FUNCTION quote_meta(text) RETURNS text AS $$
  select regexp_replace($1, '([\[\]\\\^\$\.\|\?\*\+\(\)])', '\\\1', 'g');
$$ language sql strict immutable;

CREATE FUNCTION multi_replace(str text, substitutions jsonb)
RETURNS text
as $$
DECLARE
 rx text;
 s_left text;
 s_tail text;
 res text:='';
BEGIN
 select string_agg(quote_meta(term), '|' )
 from jsonb_object_keys(substitutions) as x(term)
   where term <> ''
 into rx;

 if (coalesce(rx, '') = '') then
   -- the loop on the RE can't work with an empty alternation
   return str;
 end if;

 rx := concat('^(.*?)(', rx, ')(.*)$'); -- match no more than 1 row   

 loop
   s_tail := str;
   select 
       concat(matches[1], substitutions->>matches[2]),
       matches[3]
    from
      regexp_matches(str, rx, 'g') as matches
    into s_left, str;
    
   exit when s_left is null;
   res := res || s_left;

 end loop;

 res := res || s_tail;
 return res;

END
$$ LANGUAGE plpgsql strict immutable;

INSERT INTO annuaire
(
	--  annuaire_id autoincrement    - autoincrement 
        -- insert_datetime          - default = now
        -- update_datetime          - default = NULL                       
	search
        , type_identifiant_PP                             
        , identifiant_PP                                  
        , identification_nationale_PP                     
        , code_civilite_exercice                          
        , libelle_civilite_exercice                       
        , code_civilite                                   
        , libelle_civilite                                
        , nom                                             
        , nom_normalise                                             
        , prenom                                          
        , prenom_normalise                                          
        , code_profession                                 
        , libelle_profession                              
        , libelle_profession_normalise                              
        , code_categorie_professionnelle                  
        , libelle_categorie_professionnelle               
        , code_type_savoirfaire                           
        , libelle_type_savoirfaire                        
        , code_savoirfaire                                
        , libelle_savoirfaire                             
        , libelle_savoirfaire_normalise                             
        , code_mode_exercice                              
        , libelle_mode_exercice                           
        , siret                                           
        , siren                                           
        , finess                                          
        , finess_etablissement_juridique                  
        , id_technique_structure                          
        , raison_sociale                                  
        , enseigne_com                                    
        , complement_destinataire                         
        , complement_point_geographique                   
        , numero_voie                                     
        , indice_repetition_voie                          
        , code_type_de_voie                               
        , libelle_type_de_voie                            
        , libelle_voie                                    
        , mention_distribution                            
        , bureau_cedex                                    
        , code_postal                                     
        , code_commune                                    
        , libelle_commune                                 
        , libelle_commune_normalise                                 
        , code_pays                                       
        , libelle_pays                                    
        , telephone                                       
        , telephone_2                                     
        , telecopie                                       
        , email                                           
        , code_departement                                
        , libelle_departement                             
        , ancien_id_structure                             
        , autorite_enregistrement                         
        , code_secteur_activite                           
        , libelle_secteur_activite                        
        , code_section_tableau_pharmaciens                
        , libelle_section_tableau_pharmaciens             

        , code_type_carte                                 
        , libelle_carte                                   
        , numero_carte                                    
        , id_national_carte                               
        , debut_valid_date                                
        , fin_valid_date                                  
        , opposition_date                                 
        , miseajour_date                                  
)

SELECT 
	coalesce(multi_replace(lower(public.unaccent(p.nom)), '{" ": "", "-":""}'::jsonb), '')  || coalesce(multi_replace(lower(public.unaccent(p.prenom)), '{" ": "", "-":""}'::jsonb), '')  || coalesce(multi_replace(lower(public.unaccent(p.libelle_profession)), '{" ": "", "-":""}'::jsonb), '')  || coalesce(multi_replace(lower(public.unaccent(p.libelle_savoirfaire)), '{" ": "", "-":""}'::jsonb), '')  || coalesce(multi_replace(lower(public.unaccent(p.libelle_commune)), '{" ": "", "-":""}'::jsonb), '')
	, p.type_identifiant_pp 				
	, p.identifiant_pp 					
	, p.identification_nationale_pp 			
	, p.code_civilite_exercice 				
	, p.libelle_civilite_exercice 			
	, p.code_civilite 					
	, p.libelle_civilite 					
	, p.nom 						
	, multi_replace(lower(public.unaccent(p.nom)), '{" ": "", "-":""}'::jsonb)					
	, p.prenom 						
	, multi_replace(lower(public.unaccent(p.prenom)), '{" ": "", "-":""}'::jsonb)					
	, p.code_profession 					
	, p.libelle_profession 				
	, multi_replace(lower(public.unaccent(p.libelle_profession)), '{" ": "", "-":""}'::jsonb) 				
	, p.code_categorie_professionnelle 			
	, p.libelle_categorie_professionnelle 		
	, p.code_type_savoirfaire 				
	, p.libelle_type_savoirfaire 				
	, p.code_savoirfaire 					
	, p.libelle_savoirfaire 				
	, multi_replace(lower(public.unaccent(p.libelle_savoirfaire)), '{" ": "", "-":""}'::jsonb) 				
	, p.code_mode_exercice 				
	, p.libelle_mode_exercice 				
	, p.siret 						
	, p.siren 						
	, p.finess 						
	, p.finess_etablissement_juridique 			
	, p.id_technique_structure 				
	, p.raison_sociale 					
	, p.enseigne_com 					
	, p.complement_destinataire 				
	, p.complement_point_geographique 			
	, p.numero_voie 					
	, p.indice_repetition_voie 				
	, p.code_type_de_voie 				
	, p.libelle_type_de_voie 				
	, p.libelle_voie 					
	, p.mention_distribution 				
	, p.bureau_cedex 					
	, p.code_postal 					
	, p.code_commune 					
	, p.libelle_commune 					
	, multi_replace(lower(public.unaccent(p.libelle_commune)), '{" ": "", "-":""}'::jsonb) 					
	, p.code_pays 					
	, p.libelle_pays 					
	, p.telephone 					
	, p.telephone_2 					
	, p.telecopie 					
	, p.email 						
	, p.code_departement 					
	, p.libelle_departement 				
	, p.ancien_id_structure 				
	, p.autorite_enregistrement 				
	, p.code_secteur_activite 				
	, p.libelle_secteur_activite 				
	, p.code_section_tableau_pharmaciens 			
	, p.libelle_section_tableau_pharmaciens 		
	
	, carte.code_type_carte 				
	, carte.libelle_carte 				
	, carte.numero_carte 					
	, carte.id_national_carte 				
	, carte.debut_valid_date       			
	, carte.fin_valid_date 				
	, carte.opposition_date  				
	, carte.miseajour_date 				
FROM external_prod.ps_libreacces_personne_activite p
LEFT JOIN external_prod.porteurs_cps_cpf carte USING (identification_nationale_pp);


/************************

Create Index

*************************/

CREATE INDEX idx_annuaire_search ON annuaire USING GIN (search public.gin_trgm_ops);
CREATE INDEX idx_annuaire_prenom ON annuaire USING GIN (prenom public.gin_trgm_ops);
CREATE INDEX idx_annuaire_nom ON annuaire USING GIN (nom public.gin_trgm_ops);

CREATE INDEX idx_annuaire_commune ON annuaire  (libelle_commune ASC);
CREATE INDEX idx_annuaire_profession ON annuaire  (libelle_profession ASC);

