ALTER TABLE annuaire ADD CONSTRAINT xpk_annuaire_table PRIMARY KEY (annuaire_id);
CREATE SEQUENCE annuaire_id_seq OWNED BY annuaire.annuaire_id;
ALTER TABLE annuaire ALTER COLUMN annuaire_id SET DEFAULT nextval('annuaire_id_seq');

ALTER TABLE annuaire ALTER COLUMN insert_datetime SET DEFAULT now();
ALTER TABLE annuaire ALTER COLUMN update_datetime SET DEFAULT NULL;
