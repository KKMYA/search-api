
from flask_restful import Resource, reqparse
from flask import request
import json
from ..models.materialized_view import SearchModel


class Practitioners(Resource):
    parser = reqparse.RequestParser()
    # https://flask-restful.readthedocs.io/en/0.3.3/reqparse.html : explication for location
    # Ressource specific
    parser.add_argument('lastname',                     # GET and POST
    		type=str, location=['form', 'args'],
    		help=" (str)"
    )

    @classmethod
    def get(cls):

        #data = request.args.get('json')
        # data = json.loads(data)
        data = cls.parser.parse_args()
        
        search = SearchModel.find_from_payload(data)
				
        return search, 200

