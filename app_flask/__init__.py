import os

from flask import Flask
from flask_restful import Api
from flask_cors import CORS
from .config import TestingConfig
from .resources.practitioner import Practitioners
from .resources.helloworld import Helloworld

from .blocklist import BLOCKLIST

# Create app and API
def create_app(test_config=None):
        app = Flask(__name__, instance_relative_config=True)
#       app.config.from_envvar('APPLICATION_SETTINGS')

        if test_config is None:
                # load the instance config, if it exists, when not testing
                app.config.from_object(TestingConfig())
        else:
                # load the test config if passed in
                app.config.from_mapping(test_config)

        api = Api(app)
        # ensure the instance folder exists
        try:
                os.makedirs(app.instance_path)
        except OSError:
                pass

        # All the endpoints
        api.add_resource(Helloworld, '/')
        api.add_resource(Practitioners, '/Practitioner')

        # if __name__ == '__main__':
        from .db import db
        import logging
        
        logging.basicConfig(filename='error.log', level=logging.DEBUG)
        
        db.init_app(app)
        # endif

        # Open cross url
        cors = CORS(app, ressources={r"*": {"origins": "*"}})
        
        return app
