class Config(object):
    SECRET_KEY = "secret_key" # paste here the value of secret_key

    SQLALCHEMY_TRACK_MODIFICATIONS = False
    PROPAGATE_EXCEPTIONS = True

    USER = 'toobib' # paste here the same value of USER declared in the Makefile
    PWD = 'WVpjLrMuYcXPTnLFR' # paste here the same value of PWD declared in the Makefile
    DB = 'toobib' # paste here the same value of DB declared in the Makefile
    EXTERNAL_DIRECTORY_SCHEMA = 'external' # paste here the same value of SCHEMA declared in the Makefile
    
    POSTGRES = {
        'user': USER,
        'pwd': PWD,
        'db': DB,
        'host': 'localhost',
        'port': '5432',
    
    }
    SQLALCHEMY_DATABASE_URI = 'postgresql://%(user)s:%(pwd)s@%(host)s:%(port)s/%(db)s' % POSTGRES
    PROPAGATE_EXCEPTIONS = True
    
    # JWT CONF
    JWT_SECRET_KEY = "jwt_secret_key" # paste here the value of jwt_secret_key
    JWT_TOKEN_LOCATION = ['headers', 'query_string']    
    
    SESSION_COOKIE_SECURE = True

class ProductionConfig(Config):
    pass

class DevConfig(Config):
    DEBUG = True
    SESSION_COOKIE_SECURE = False

class TestingConfig(Config):
    TESTING = True
    SESSION_COOKIE_SECURE = False

