from unidecode import unidecode 
import re
my_dict = {
	"libelleCommune" : "libelle_commune",
	"libelleProfession" : "libelle_profession",
}

def normalizeStrings(dico):
    """
        erase '-', lower()
        unidecode : unidecode('ko\u017eu\u0161\u010dek') -> 'kozuscek'
        del empty key for dict
    """
    new_dico = {}
    for k in dico:
        if dico[k] and type(dico[k]) is str:
            new_dico[k] = unidecode(re.sub(r"[-]","", dico[k])).lower()
        elif dico[k]: # if strings[key] is None -> del
            new_dico[k] = dico[k]
    return new_dico
