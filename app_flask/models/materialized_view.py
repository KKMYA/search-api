from ..config import Config
from ..util import normalizeStrings
from ..db import db
from sqlalchemy.orm import load_only
from sqlalchemy import or_
from unidecode import unidecode 
import re

class SearchModel(db.Model):
    __tablename__ = 'recherche'
    __table_args__ = ({'schema': Config.EXTERNAL_DIRECTORY_SCHEMA})

    inpp = db.Column(db.Text(), primary_key=True)
    nom = db.Column(db.Text())
    nom_normalise = db.Column(db.Text())
    prenom = db.Column(db.Text())
    prenom_normalise = db.Column(db.Text())
    profession = db.Column(db.Text())
    profession_normalise = db.Column(db.Text())
    savoir_faire = db.Column(db.Text())
    savoir_faire_normalise = db.Column(db.Text())
    commune = db.Column(db.Text())
    commune_normalise = db.Column(db.Text())

    def __init__(self, identifier, firstname, lastname, job, city):
        self.inpp = identifier
        self.prenom = firstname.title()
        self.nom = lastname.title()
        self.profession = job.lower()
        self.commune = city.lower()

    def json(self):
        return {
            'inpp': self.inpp,
            'prenom': self.prenom,
            'nom': self.nom,
            'commune': self.commune,
            'profession': self.profession,
            'savoirFaire': self.savoir_faire,
        }

    @classmethod
    def find_from_payload(cls, data):
        print(data.lastname)
        return cls.query.filter_by(nom=data.lastname).first().json()
    
    @classmethod
    def find_by_inpp(cls, identifier):
        return cls.query.filter_by(inpp=identifier).first()
#
#    @classmethod
#    def normalize_check_payload(cls, dico):
#        """
#            data_base <-> FHIR terms
#
#            if str :
#                - erase '-', lower()
#                - unidecode : unidecode('ko\u017eu\u0161\u010dek') -> 'kozuscek'
#            del empty key for dict
#
#            return new_dico, page, limit
#
#            pop = https://kite.com/python/answers/how-to-rename-a-dictionary-key-in-python
#            change javascript standard variable declaration to PEP
#        """
#        normalize_dico = {
#            "identifier" : "profession_normalise",
#            "address-city" : "commune_normalise",
#            "family" : "nom_normalise",
#            "given" : "prenom_normalise",
#            "specialty" : "savoir_faire_normalise",
#            "role" : "profession_normalise",
#            "identifier" : "inpp",
#        }
#
#        new_dico = {}
#        page = 1
#        limit = 12
#
#        # Erase all keys that should not be processed by bundle_search function
#        if "_page_" in dico and dico['_page_']:
#            page = dico['_page_']
#            del dico["_page_"]
#        if "_count" in dico and dico['_count']:
#            limit = dico['_count']
#            del dico["_count"]
#
#        # Fhir to database traduction
#        for k in dico:
#            if type(dico[k]) is str:
#                dico[k] = unidecode(re.sub(r"[-]","", dico[k])).lower()
#            if dico[k]: # if strings[key] is None -> del
#                if k in normalize_dico:
#                    new_dico[normalize_dico[k]] = dico[k]
#                else:
#                    new_dico[k] = dico[k]
#
#       
#        return new_dico, page, limit
#    
#        
#    @classmethod
#    def bundle_search_practitioner(cls, data, page, limit):
#        """
#            Find providers in annuaire table
#            Use GIN index of postgreSQL, cf READ.md
#            If search parameter is filled others are ignored
#
#            Use Paginate function : https://flask-sqlalchemy.palletsprojects.com/en/2.x/api/#flask_sqlalchemy.Pagination
#        """
#        words = []
#        query_set = cls.query
#
#        if '_content' in data:
#            words += data['_content'].split(' ')
#            del data['_content']
#        if 'name' in data:
#            words += data['name'].split(' ')
#            del data['name']
#        for w in words:
#                query_set = query_set.filter(or_(
#                    cls.prenom_normalise.like('%' + w + '%'),
#                    cls.nom_normalise.like('%' + w + '%')))
#                    # cls.commune_normalise.like('%' + w + '%'),
#
#        # for nom, prenom, commune, profession, identifiant_pp
#        for k, v in data.items() :
#            if v:
#                query_set = query_set.filter(getattr(cls, k).like('%' + v + '%'))
#
#        # pagination
#        return query_set.distinct(getattr(cls, 'inpp')).from_self().order_by(SearchModel.nom).paginate(page=page,
#                per_page=limit, error_out=True, max_per_page=limit)
#
#    @classmethod
#    def bundle_search_practitioner_role(cls, data, page, limit):
#        """
#            Find providers in annuaire table
#            Use GIN index of postgreSQL, cf READ.md
#            If search parameter is filled others are ignored
#
#            Use Paginate function : https://flask-sqlalchemy.palletsprojects.com/en/2.x/api/#flask_sqlalchemy.Pagination
#        """
#        query_set = cls.query
#        # if _id is present all other params are ignored
#
#        if '_content' in data:  # for full search
#            words = data['_content'].split(' ')
#            del data['_content']
#            for w in words:
#                query_set = query_set.filter(or_(
#                    cls.prenom_normalise.like('%' + w + '%'),
#                    cls.nom_normalise.like('%' + w + '%'),
#                    cls.commune_normalise.like('%' + w + '%'),
#                    cls.profession_normalise.like('%' + w + '%'),
#                    cls.savoir_faire_normalise.like('%' + w + '%')))
#
#        # for nom, prenom, commune, profession, savoir_faire
#        for k, v in data.items() :
#            if v:
#                query_set = query_set.filter(getattr(cls, k).like('%' + v + '%'))
#
#        # pagination
#        return query_set.order_by(SearchModel.nom).paginate(page=page,
#                per_page=limit, error_out=True, max_per_page=limit)
#   
#    
#
#    @classmethod
#    def find_distinct(cls, column, data):
#        query_set = None
#        if data['search']:  # for full search
#            words = data['search'].split(' ')
#            for w in words:
#                if query_set:
#                    query_set = query_set.filter(or_(
#                        cls.prenom_normalise.like('%' + w + '%'),
#                        cls.commune_normalise.like('%' + w + '%'),
#                        cls.profession_normalise.like('%' + w + '%'),
#                        cls.nom_normalise.like('%' + w + '%')))
#                else:
#                    query_set = cls.query.filter(or_(
#                        cls.prenom_normalise.like('%' + w + '%'),
#                        cls.commune_normalise.like('%' + w + '%'),
#                        cls.profession_normalise.like('%' + w + '%'),
#                        cls.nom_normalise.like('%' + w + '%')))
#        else:
#            query_set = cls.query
#        # https://stackoverflow.com/questions/11530196/flask-sqlalchemy-query-specify-column-names
#        return query_set.distinct(getattr(cls, column)).all()
